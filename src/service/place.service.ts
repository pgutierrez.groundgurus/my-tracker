import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

import { Place } from '../model/new-place.model';

@Injectable()
export class PlaceService {
    places: Place[] = [];

    constructor(private storage: Storage) {
    }

    getPlaces() {
        // return this.places.slice();
        return this.storage.get('places').then(places => {
            this.places = places == null ? [] : places;
            return this.places;
        });
    }

    addPlace(place: Place) {
        this.places.push(place);
        this.storage.set('places', this.places);
    }

    deletePlace(index: number) {
        this.places.splice(index, 1);
        this.storage.set('places', this.places);
    }
}