import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Place } from '../../model/new-place.model';

import { PlaceService } from '../../service/place.service';
import { NewPlacePage } from "../new-place/new-place";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  lat: number = 51.678418;
  lng: number = 7.809007;

  places: Place[] = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, private placeService: PlaceService) {
  }

  ionViewDidEnter() {
    this.initPlaces();
  }

  initPlaces() {
    this.placeService.getPlaces().then(places => this.places = places);
  }

  newPlace() {
    this.navCtrl.push(NewPlacePage);
  }

  deletePlace(index: number) {
    this.placeService.deletePlace(index);

    // update the places after removing
    this.initPlaces();
  }
}
